#! /usr/bin/env zsh

export XDG_CONFIG_HOME=~/.config
export ZDOTDIR=~/.config/zsh
export ZSH_PLUGINS=/usr/share/zsh/plugins


# set up plugin directory
[ ! -d $ZSH_PLUGINS ] && sudo mkdir -p $ZSH_PLUGINS


# minimal envivars for zsh on tty
if [ -f /etc/zsh/zshenv ]; then
   sudo cp /etc/zsh/zshenv /etc/zsh/zshenvbak_$(date +%m-%d-%y)

   echo "#! /usr/bin/env zsh\n" | sudo tee /etc/zsh/zshenv > /dev/null
   echo "export XDG_CONFIG_HOME=~/.config" | sudo tee -a /etc/zsh/zshenv > /dev/null
   echo "export ZDOTDIR=~/.config/zsh" | sudo tee -a /etc/zsh/zshenv > /dev/null
   echo "export ZSH_PLUGINS=/usr/share/zsh/plugins" | sudo tee -a /etc/zsh/zshenv > /dev/null
fi


# set install command
if [[ $(uname -n) = *arch* ]]; then
   _install='sudo pacman -S --needed --noconfirm '
else
   _install='sudo apt install '
fi


dependencies=(
   bat
   bc
   eza
   figlet
   lolcat
   pandoc
   neofetch
   fzf
   lynx
)

for package in $dependencies; do
   eval "${_install}" $package
done


# build from source
if [ ! -d $ZSH_PLUGINS/zsh-syntax-highlighting ]; then
   sudo git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_PLUGINS/zsh-syntax-highlighting
fi

if [ ! -d $ZSH_PLUGINS/zsh-autosuggestions ]; then
   sudo git clone git clone https://github.com/zsh-users/zsh-autosuggestions $ZSH_PLUGINS/zsh-autosuggestions
fi

# figlet font 
if [ ! -f /usr/share/figlet/fonts/ANSIShadow.flf ]; then
   sudo curl https://raw.githubusercontent.com/xero/figlet-fonts/master/ANSIShadow.flf -o /usr/share/figlet/fonts/ANSIShadow.flf
fi

# change default shell to zsh
if [[ $(basename $SHELL) != 'zsh' ]]; then
   chsh -s $(which zsh)
   sudo chsh -s $(which zsh)
fi

echo -e "\n\e[32mDone.\e[0m\n"
