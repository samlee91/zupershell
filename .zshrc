# modules
autoload -Uz colors && colors
autoload -Uz compinit && compinit

# history
HISTSIZE=5000
SAVEHIST=5000
HISTFILE=$ZDOTDIR/.history
setopt EXTENDED_HISTORY
setopt INC_APPEND_HISTORY

# functions
fpath=( $ZDOTDIR/functions "${fpath[@]}" )
autoload -Uz $fpath[1]/*(.:t)

function safe_load() {
   [ -f $1 ] && source $1
}

# startup
safe_load $ZSH_PLUGINS/zsh-autosuggestions/zsh-autosuggestions.zsh
safe_load $ZSH_PLUGINS/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
safe_load $ZDOTDIR/aliases.zsh
safe_load $ZDOTDIR/stdout.zsh
safe_load $ZDOTDIR/plugins/autosuggestions.zsh
safe_load $ZDOTDIR/plugins/syntax_highlighting.zsh

# tty
if [ -z "$XDG_CURRENT_DESKTOP" ]; then
   NEWLINE=$'\n'
   PS1="$NEWLINE%F{magenta}%n%F{yellow}@%F{cyan}%m %F{blue}$ %f"
   echo && pfetch
   return
fi

safe_load $ZDOTDIR/prompts/pacman_prompt.zsh
neofetch
