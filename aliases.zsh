
# sourcing
alias sz="source $ZDOTDIR/.zshrc"

# terminal
alias :q="exit"
alias :wq="exit"
alias c="clear"

# system
alias sa="systemd-analyze"
alias gpu="lspci -v -s 07:00.0"
alias gputemp="sensors | grep -A10 amdgpu-pci | grep -oP \"(?<=^).*(?=C  \()\""

# ls
alias ls="echo && eza --color-scale-mode=fixed --icons=always --group-directories-first --sort=extension --time-style='+%e %b'"
alias l="ls"
alias ll="ls -l"
alias la="ls -a"
alias lal="ls -al"
alias tree="ls --tree"

# nvim
alias zshrc="nvim $ZDOTDIR/.zshrc"
alias nvimp="nvim -p"
alias snvim="sudo -E nvim"
alias snvimp="sudo -E nvim -p"
alias nvimall="nvim -p *"
alias snvimall="sudo nvim -p *"

# package mgmt
alias p="pacman"
alias sp="sudo pacman"
alias sps="sudo pacman -S"
alias pq="pacman -Q"
alias yq="yay -Q"
alias pss="pacman -Ss"
alias yss="yay -Ss"
alias ys="yay -S"
alias prm="sudo pacman -Runs"
alias yrm="yay -Runs"
alias ukr="sudo pacman -Sy archlinux-keyring"

# programs
alias bc="print -P \"\n%F{1} %F{11} %F{3} %F{2} %F{6} %F{4} %F{5} %f\" && bc -lq "
alias lolcat="lolcat --freq=0.3"
alias docedit="libreoffice --writer"
alias nf="neofetch"
alias mobofetch="neofetch --config ~/.config/neofetch/mobofetch.conf"
alias pipes="pipes.sh -p 4 -c 2 -c 4 -c 5 -c 6"

# git
alias gst="git status"
alias gcm="git commit -m"
alias ga="git add"

# timeshift
alias tsl="sudo timeshift --list --snapshot-device /dev/nvme0n1p2"
alias tsc="sudo timeshift --create --comments"
alias tsrm="sudo timeshift --delete --snapshot"

# programming
alias julia="julia --banner=no"

# screenshots
alias screenshot='grim -g "$(slurp)"'

# colors
alias cat="bat"
alias grep="grep --color=auto"
alias diff="diff --color=auto"
alias yay="yay --color=always"
alias ip="ip -color=auto"
