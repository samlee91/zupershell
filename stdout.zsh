#! /usr/bin/env zsh

# This file applies color to stdout text
# tips:  https://wiki.archlinux.org/title/Color_output_in_console

# ansi codes
bold=1
faint=2
italic=3
black=30
red=31
green=32
yellow=33
blue=34
magenta=35
cyan=36
white=37
orange=93



#  ██████╗  █████╗ ████████╗
#  ██╔══██╗██╔══██╗╚══██╔══╝
#  ██████╔╝███████║   ██║   
#  ██╔══██╗██╔══██║   ██║   
#  ██████╔╝██║  ██║   ██║   
#  ╚═════╝ ╚═╝  ╚═╝   ╚═╝   

export BAT_THEME=OneHalfDark
export BAT_STYLE=plain



#   ██████╗ ██████╗ ███████╗██████╗ 
#  ██╔════╝ ██╔══██╗██╔════╝██╔══██╗
#  ██║  ███╗██████╔╝█████╗  ██████╔╝
#  ██║   ██║██╔══██╗██╔══╝  ██╔═══╝ 
#  ╚██████╔╝██║  ██║███████╗██║     
#   ╚═════╝ ╚═╝  ╚═╝╚══════╝╚═╝     

match="ms=${bold};${italic};${cyan}"
export GREP_COLORS=${match}



#  ██╗     ███████╗███████╗███████╗
#  ██║     ██╔════╝██╔════╝██╔════╝
#  ██║     █████╗  ███████╗███████╗
#  ██║     ██╔══╝  ╚════██║╚════██║
#  ███████╗███████╗███████║███████║
#  ╚══════╝╚══════╝╚══════╝╚══════╝

export LESS_TERMCAP_md=$(tput bold; tput setaf 4) # bold text -> blue
export LESS_TERMCAP_so=$(tput bold; tput setaf 2) # start standout -> green
export LESS_TERMCAP_se=$(tput rmso; tput sgr0)    # end standout
export LESS_TERMCAP_us=$(tput smul; tput bold; tput setaf 5) # underlined text -> magenta
export LESS_TERMCAP_me=$(tput sgr0) # reset all



#  ███████╗███████╗ █████╗ 
#  ██╔════╝╚══███╔╝██╔══██╗
#  █████╗    ███╔╝ ███████║
#  ██╔══╝   ███╔╝  ██╔══██║
#  ███████╗███████╗██║  ██║
#  ╚══════╝╚══════╝╚═╝  ╚═╝
# documentation: man 5 eza_colors

typeset -A eza_colors=(
   [ur]="$yellow"                 # user read permission bit
   [uw]="$magenta"                # user write permission bit
   [ux]="$green"                  # user execute permission bit (regular files)
   [ue]="$green"                  # user execute permission bit (other files)
   [gr]="$yellow"                 # group read permission bit
   [gw]="$magenta"                # group write permission bit
   [gx]="$green"                  # group execute permission bit
   [tr]="$yellow"                 # other read permission bit
   [tw]="$magenta"                # other write permission bit
   [tx]="$green"                  # other execute permission bit
   [uu]="$bold;$italic;$yellow"   # user name (self)
   [uR]="$bold;$italic;$red"      # user name (root)
   [sn]="$bold;$cyan"             # file size numbers
   [sb]="$bold;$cyan"             # file size units
   [di]="$bold;$blue"             # directories
   [ln]="$bold;$cyan"             # symlinks
   [lp]="$cyan"                   # symlink paths
   [da]="$magenta"                # date
   [do]="$orange"                 # document files (pdf, office, etc.)
   [co]="$red"                    # compressed files
   [bu]="$bold;$yellow"           # build files (Makefile, etc.)

   # filetype globs
   [*.bak]="$faint;$white"  # backup files
   [*.c]="$blue"            # c files
   [*.cpp]="$blue"          # c++ files
   [*.css]="$red"           # css files
   [*.scss]="$red"          # scss files
   [*.gitlab*]="$orange"    # git directory
   [*.hex]="$magenta"       # hex binaries
   [*.h]="$yellow"          # header files
   [*.js]="$magenta"        # javascript files
   [*.json]="$magenta"      # JSON files
   [*.lua]="$cyan"          # lua files
   [*.py]="$yellow"         # python files
   [*.sh]="$green"          # shell scripts
   [*.txt]="$white"         # text files
   [*.zsh]="$green"         # zsh scripts
)

EZA_COLORS=""
for x in ${(k)eza_colors}; do
   EZA_COLORS+=":${x}=${eza_colors[$x]}"
done

export EZA_COLORS="${EZA_COLORS:1}"
export EZA_ICON_SPACING=1
