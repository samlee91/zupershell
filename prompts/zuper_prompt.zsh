#! /usr/bin/env zsh

# Colors
dir_icon_color="blue"
dir_name_color="blue"
root_background="red"
git_icon_color="cyan"
git_branch_color="cyan"
git_wrapper_color="8"

# Wrapper
left_wrapper=""
right_wrapper=""
wrapper_color="8"

# Main Icons
root_icon="√ "
home_icon=" "
documents_icon=" "
downloads_icon=" "
shell_icon=" "
config_icon=" "
hardware_icon=" "
vim_icon=" "
math_icon=" "
school_icon=" "


# Programming Icons
code_icon=" "
python_icon=" "
cpp_icon=" "
julia_icon=" "
java_icon=" "
rust_icon=" "
lua_icon=" "
git_icon=" "

# Pointer
arrow_icon="❯"



function arrows() {
   print "%F{magenta}$arrow_icon%F{yellow}$arrow_icon%F{cyan}$arrow_icon "
}



function gitPrompt() {
   if [ -d .git ]; then
      branch=$(basename $(git symbolic-ref HEAD))
      print " $git_icon$branch"
   fi
}


function git_branch() {
   if [ -d .git ]; then
      print $(basename $(git symbolic-ref HEAD))
   fi
}



function zuperPrompt() {
   dir=""
   case "`pwd`" in
      /)       icon=$root_icon ;;
      /home)   icon=$home_icon ;;
      $HOME)   icon=$home_icon; dir="~" ;;
      $HOME/*)

         dir=$(basename "`pwd`")

         case "`pwd`" in
            *Documents*)  icon=$documents_icon ;;
            *Downloads*)  icon=$downloads_icon ;;
            *Math*)       icon=$math_icon ;;
            *School*)     icon=$school_icon ;;
            *$ZDOTDIR*)   icon=$shell_icon ;;
            *vim*)        icon=$vim_icon ;;

            *circuits* || *EE* || *kicad )      icon=$hardware_icon ;;
            *.dotfiles* || *.config*) icon=$config_icon ;;

            *code* || *dev*)
               case "`pwd`" in
                  *c      || *c/*)      icon=$cpp_icon ;;
                  *c++    || *c++/*)    icon=$cpp_icon ;;
                  *java   || *java/*)   icon=$java_icon ;;
                  *julia  || *julia/*)  icon=$julia_icon ;;
                  *lua    || *lua/*)    icon=$lua_icon ;;
                  *python || *python/*) icon=$python_icon ;;
                  *rust   || *rust/*)   icon=$rust_icon ;;
                  *)                    icon=$code_icon ;;
               esac
               ;;

            *) icon="" ;;
         esac
         ;;

      *)
         icon=""
         dir=$(basename "`pwd`")
         ;;

   esac


   wrapper=" %B%F{$wrapper_color}$left_wrapper%f%K{$wrapper_color}"
   wrapper+="%F{$dir_icon_color}$icon%f%F{$dir_name_color}$dir%f%k"
   wrapper+="%k%F{$wrapper_color}$right_wrapper"

   declare num_chars
   git_label=$(git_branch)

   if [[ -n $git_label ]]; then
      (( num_chars = $(echo $icon$dir$git_icon$git_label | wc -m) + 1 ))

      wrapper+="%F{0}%K{$git_wrapper_color}$right_wrapper%f"
      wrapper+=" %F{$git_icon_color}$git_icon%f%F{$git_branch_color}\e[3m$git_label%f%k"
      wrapper+="%k%F{$git_wrapper_color}$right_wrapper"

   else
      (( num_chars = $(echo $icon$dir | wc -m) - 2 ))

   fi


   # draw line
   line="\n╭──"
   for i in {1..$num_chars}; do
      line+="─"
   done
   line+="──╯"

   row1="\n$wrapper─╮"
   row2=$line
   row3="\n╰─%f $(arrows)\e[0m"

   print "$row1$row2$row3"

}


function secondaryPrompt() {
   print "%F{$wrapper_color}   %f$(arrows)"
}


setopt PROMPT_SUBST
#PROMPT='$(/home/sam/Code/C/prettyprompt/prettyprompt)'
PROMPT='$(zuperPrompt)'
PS2='$(secondaryPrompt)'

