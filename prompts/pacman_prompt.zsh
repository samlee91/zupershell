#! /usr/bin/env zsh

# options
wrapped="true"
home_dir="~"

# colors
wrapper="blue"
directory="black"
ghost=( "magenta" "cyan" "green" )

# icons
dir_icon=" "
normal_pointer=" "
git_pointer=" "
pyvenv_pointer=" "


typeset -gA path_icons
path_icons[$HOME]=" "
path_icons[$HOME/Documents]=" "
path_icons[$HOME/Downloads]=" "
path_icons[$HOME/Pictures]=" "
path_icons[$XDG_CONFIG_HOME*]=" "
path_icons[$XDG_CONFIG_HOME/nvim]=" "




pacman="%F{yellow}%K{yellow}%F{black}%k%f"
pacman_chomp="%F{yellow}%f"
dots="%F{white}• • • %f"
ghosts="%F{${ghost[1]}} %F{${ghost[2]}} %F{${ghost[3]}} "
ghost1="%F{${ghost[1]}} %f"
ghost2="%F{${ghost[2]}} %f"
ghost3="%F{${ghost[3]}} %f"



function init_vars() {
   [ -d .git ] && PROMPT_POINTER=$git_pointer || PROMPT_POINTER=$normal_pointer
   [ $VIRTUAL_ENV ] && PROMPT_POINTER=$pyvenv_pointer

}


function pacman_prompt() {
   echo 0 > $ZDOTDIR/prompts/.ps2count
   init_vars
   cwd=`pwd`

   #for key value in ${(kv@)path_icons}; do
   #   [[ $cwd == ${~key} ]] && dir_icon=$value
   #done

   cwd=$(basename $cwd)
   if [ $cwd = $USER ] && [ $home_dir ]
      then cwd=$home_dir
   fi
   if [ $wrapped ]
      then cwd="%F{${wrapper}}%F{${directory}}%K{${wrapper}}%B${dir_icon}${cwd}%k%b%F{${wrapper}}%f"
      else cwd="%F{${directory}}%B${dir_icon}${cwd}%b%f"
   fi
   print "\n%F{${wrapper}}╭─────${cwd}\n%F{${wrapper}}╰─ ${pacman}${dots}${ghosts}%F{${wrapper}}──${PROMPT_POINTER}%f "
}


function secondary_prompt() {
   init_vars
   ps2_pacmans=(
      "%F{${wrapper}} ── ${pacman_chomp}%F{white} • • %f${ghosts}%F{${wrapper}}──${PROMPT_POINTER}%f "
      "%F{${wrapper}} ─── ${pacman}%F{white}• • %f${ghosts}%F{${wrapper}}──${PROMPT_POINTER}%f "
      "%F{${wrapper}} ──── ${pacman_chomp}%F{white} • %f${ghosts}%F{${wrapper}}──${PROMPT_POINTER}%f "
      "%F{${wrapper}} ───── ${pacman}%F{white}• %f${ghosts}%F{${wrapper}}──${PROMPT_POINTER}%f "
      "%F{${wrapper}} ────── ${pacman_chomp} %f${ghosts}%F{${wrapper}}──${PROMPT_POINTER}%f "
      "%F{${wrapper}} ─────── ${pacman}${ghosts}%F{${wrapper}}──${PROMPT_POINTER}%f "
      "%F{${wrapper}} ──────── ${pacman_chomp} ${ghost2}${ghost3}%F{${wrapper}}──${PROMPT_POINTER}%f "
      "%F{${wrapper}} ───────── ${pacman}${ghost2}${ghost3}%F{${wrapper}}──${PROMPT_POINTER}%f "
      "%F{${wrapper}} ────────── ${pacman_chomp} ${ghost3}%F{${wrapper}}──${PROMPT_POINTER}%f "
      "%F{${wrapper}} ─────────── ${pacman}${ghost3}%F{${wrapper}}──${PROMPT_POINTER}%f "
      "%F{${wrapper}} ──────────── ${pacman_chomp}%F{${wrapper}} ──%F{red} %f"
      "%F{${wrapper}} ───────────── ${pacman}%F{${wrapper}} ─%F{red} %f"
      "%F{${wrapper}} ────────────── ${pacman_chomp}%F{${wrapper}} %F{red} %f"
      "%F{${wrapper}} ─────────────── ${pacman}%F{${wrapper}}%F{red} %f"
      "%F{${wrapper}} ──────────────── %B%F{white}100 %b%f"
      "%F{${wrapper}} ─ ${pacman}%F{white}• • • %f${ghosts}%F{${wrapper}}──${PROMPT_POINTER}%f "
   )

   newline_count=$(cat $ZDOTDIR/prompts/.ps2count)
   (( newline_count = newline_count % $#ps2_pacmans + 1 ))
   echo $newline_count > $ZDOTDIR/prompts/.ps2count
   print $ps2_pacmans[$newline_count]
}


setopt PROMPT_SUBST
VIRTUAL_ENV_DISABLE_PROMPT=1
PROMPT='$(pacman_prompt)'
PS2='$(secondary_prompt)'

