#! /usr/bin/env zsh
# https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/docs/highlighters.md

#setopt INTERACTIVE_COMMENTS

# Active Highlight Groups
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main pattern cursor root)
typeset -gA ZSH_HIGHLIGHT_STYLES


ZSH_HIGHLIGHT_STYLES=(
   [path]="fg=white,bold"
   [arg0]="fg=blue"
   [reserved-word]="fg=magenta"
   [function]="fg=blue"
   [precommand]="fg=magenta"
   [alias]="fg=blue"
   [command]="fg=blue"
   [command-substitution-delimiter]="fg=yellow"
   [process-substitution]="fg=yellow"
   [process-substitution-delimiter]="fg=yellow"
   [double-hyphen-option]="fg=cyan"
   [single-hyphen-option]="fg=yellow"
   [redirection]="fg=magenta"
   [single-quoted-argument]="fg=green"
   [double-quoted-argument]="fg=green"
   [back-quoted-argument]="fg=cyan"
   [globbing]="fg=white,bold"
   [comment]="fg=8"

   # Brackets (must have `brackets` in ZSH_HIGHLIGHT_HIGHLIGHTERS)
   [bracket-error]="none"
   [bracket-level-1]="8"
   [bracket-level-2]="8"
   [bracket-level-3]="8"
   [bracket-level-4]="8"
   [bracket-level-5]="8"
   [cursor-matchingbracket]="standout"
)

# Operators and Numbers
ZSH_HIGHLIGHT_PATTERNS+=(
   "&" "fg=magenta"
   "\|" "fg=magenta"
   "\!" "fg=magenta"
   "+" "fg=magenta"
   "\-" "fg=magenta"
   "=" "fg=magenta"
   "[0-9]" "fg=11"
   "\[" "fg=cyan,bold"
   "\]" "fg=cyan,bold"
)
