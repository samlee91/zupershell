#! /usr/bin/env zsh

ZSH_AUTOSUGGEST_STRATEGY=( history completion )

#  ignore commands longer than 30 chars
ZSH_AUTOSUGGEST_HISTORY_IGNORE="?(#c100,)"
ZSH_AUTOSUGGEST_MAX_BUFFER_SIZE=20

bindkey "^[^M" autosuggest-accept
#bindkey "^ " history-beginning-search-backward
